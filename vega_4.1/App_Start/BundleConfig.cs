﻿using System.Web;
using System.Web.Optimization;

namespace vega_4._1
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/operator/master").Include(
                      "~/Scripts/master_operator.js"));

            bundles.Add(new StyleBundle("~/ContentOperator/css").Include(
                      "~/Content/joint.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/operator.css"));

            bundles.Add(new StyleBundle("~/ContentAdmin/css").Include(
                     "~/Content/joint.css",
                     "~/Content/font-awesome.min.css",
                     "~/Content/admin.css"));

            bundles.Add(new StyleBundle("~/ContentLk/css").Include(
                      "~/Content/joint.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/lk.css"));

            bundles.Add(new StyleBundle("~/ContentAccount/css").Include(
                      "~/Content/joint.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/account.css"));
        }
    }
}
