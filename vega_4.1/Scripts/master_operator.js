﻿"use strict";

document.addEventListener("DOMContentLoaded", function (event) {

    /* Объявление глобальных переменных */

    let btnOpenMenu = document.getElementById("open-menu");

    let btnCloseMenu = document.getElementById("close-menu");

    let containerContacts = document.getElementsByClassName("container-contacts");

    let boxMessage = document.getElementById("box-message");

    let btnMessageClose = document.getElementById("close-dialog");

    let btnUpListTech = document.getElementById("btn-up-list-tech");

    let btnDownListTech = document.getElementById("btn-down-list-tech");


    /* Блок обработчиков кнопок */

    for (let i = 0; i < containerContacts.length; i++) {
        containerContacts[i].onclick = function () {
            boxMessage.style.display = "flex";

            if (localStorage.getItem("op_menu") != "true") {
                localStorage.setItem("op_menu", "true");
            }

        }
    }

    btnMessageClose.onclick = function () {

        boxMessage.style.display = "none";
        localStorage.setItem("op_menu", "false");

    }

    btnOpenMenu.onclick = function () {

        let navMenu = document.getElementById("nav-menu");
        navMenu.style.display = "block";
        navMenu.style.left = "0px";


    };

    btnCloseMenu.onclick = function () {

        let navMenu = document.getElementById("nav-menu");
        navMenu.style.left = "-220px";
        navMenu.style.display = "none";

    };

    btnUpListTech.onclick = function () {

        let hListTech = document.getElementById("list-technics").clientHeight;

        let hBody = document.getElementById("body-list-tech").clientHeight;

        console.log(hListTech);

        console.log(hBody);

        if (hBody <= hListTech) {

            let initial = document.getElementById("list-technics").style.top;

            if (initial == false || initial == null) {

                document.getElementById("list-technics").style.top = "-20px";

            }
            else {

                let value = document.getElementById("list-technics").style.top;

                let number = Number.parseInt(value.substring(1, value.length - 1));

                number += 20;

                if (number >= (hListTech - hBody) + 20) {

                    number = (hListTech - hBody) + 20;

                }

                document.getElementById("list-technics").style.top = "-" + number.toString() + "px";

                console.log(document.getElementById("list-technics").style.top);

            }

        }

    };

    btnDownListTech.onclick = function () {

        let hListTech = document.getElementById("list-technics").clientHeight;

        let hBody = document.getElementById("body-list-tech").clientHeight;

        console.log(hListTech);

        console.log(hBody);

        if (hBody <= hListTech) {

            let value = document.getElementById("list-technics").style.top;

            let number = Number.parseInt(value.substring(1, value.length - 1));

            if (number >= 0) {

                number -= 20;

                if (number <= 0) {

                    number = 0;

                }

                document.getElementById("list-technics").style.top = "-" + number.toString() + "px";

                console.log(document.getElementById("list-technics").style.top);

            }

           /* if ((document.getElementById("list-technics").style.top == "")) {

                document.getElementById("list-technics").style.top = "-0px";

            }
            else {

                let value = document.getElementById("list-technics").style.top;

                let number = Number.parseInt(value.substring(1, value.length - 1));

                number += 20;

                if (number >= (hListTech - hBody) + 20) {

                    number = (hListTech - hBody) + 20;

                }

                document.getElementById("list-technics").style.top = "-" + number.toString() + "px";

                console.log(document.getElementById("list-technics").style.top);

            }*/

        }

    }

    /* Блок объявления функций-обработчиков в зависимости от загруженной страницы */

    function hForIndex() {

        let btnOpenTimeApp = document.getElementById("open-time");

        let btnCloseTimeApp = document.getElementById("close-time");

        btnOpenTimeApp.onclick = function () {

            let navMenu = document.getElementById("block-time-app");
            navMenu.style.right = "0%";

        };

        btnCloseTimeApp.onclick = function () {

            let navMenu = document.getElementById("block-time-app");
            navMenu.style.right = "-100%";

        };

    }

    function hForHistory() {

    }

    function hForMap() {

    }


    /* Получаем URL */

    let path = window.location.pathname;

    let arrayPath = [(path.split('/'))[1], (path.split('/'))[2]];


    /* Выполняем функции-обработчики в зависимости от URL */

    if (arrayPath[1] == undefined || arrayPath[1] == "" || arrayPath[1].toLowerCase() == "index") {

        hForIndex();

    }
    else if (arrayPath[1].toLowerCase() == "history") {

    }
    else if (arrayPath[1].toLowerCase() == "map") {

    }

    /* Проверка localStorage и первичная настройка */

    if (localStorage.getItem("op_menu") == "true") {
        boxMessage.style.display = "flex";
        boxMessage.style.right = "110px";
    }

});