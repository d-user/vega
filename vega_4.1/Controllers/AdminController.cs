﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace vega_4._1.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            return View();
        }

        public ActionResult Firma()
        {
            return View();
        }

        public ActionResult Street()
        {
            return View();
        }

        public ActionResult Network()
        {
            return View();
        }

        public ActionResult Object()
        {
            return View();
        }

        public ActionResult Settings()
        {
            return View();
        }

        public ActionResult Chat()
        {
            return View();
        }

    }
}